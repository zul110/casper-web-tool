// @ts-nocheck
import React, { useState, useEffect, useRef } from "react";
import { Button, Container, Col, Form, Row, Modal, InputGroup, Image } from "react-bootstrap";
import fetch from 'node-fetch';
import FormData from 'form-data';

import _urls from '../Chrome/7awiTabs/urls';
import _tools from '../OnlineTools/tools';
import _extensions from '../Chrome/Extensions/extensions';

import { AiFillCaretUp, AiFillCaretDown } from 'react-icons/ai';

import './admin.css';

interface itemToRemoveInterface {
    index: number,
    name?: string,
    title?: string
}

interface stateInterface {
    type: string,
    itemToRemove: number
}

const Admin = ({ show, toggleAdminArea, urlsUpdated, extensionsUpdated, toolsUpdated }: any) => {
    const dev = true;

    const imagesInput = useRef(null);
    const nameInput = useRef(null);
    const urlInput = useRef(null);

    const [urls, setUrls] = useState([]);
    const [extensions, setExtensions] = useState([]);
    const [tools, setTools] = useState([]);

    const [state, setState] = useState<stateInterface>({ type: 'url', itemToRemove: -1 });
    const [urlsState, setUrlsState] = useState({ urls, name: '', url: '', images: [] });
    const [extensionsState, setExtensionsState] = useState({ extensions, name: '', url: '', images: [] });
    const [toolsState, setToolsState] = useState({ tools, name: '', url: '', images: [] });

    const [showConfirmation, setShowConfirmation] = useState(false);

    const handleClose = () => setShowConfirmation(false);
    const handleShow = () => setShowConfirmation(true);

    useEffect(() => {
        (async () => {
            setUrls(await _urls.getData());
            setExtensions(await _extensions.getData());
            setTools(await _tools.getData());
        })();
    }, []);

    const addItem = async () => {
        let body = new FormData();

        switch (state.type) {
            case 'url':
                body.append('name', urlsState.name);
                body.append('url', urlsState.url);
                body.append('images', urlsState.images[0]);
                // body.append('images', fs.createReadStream(urlsState.images), { filename: urlsState.images.name });

                fetch(dev ? 'http://localhost:8000/urls' : 'https://feedsv2.7awi.com/casper/urls', {
                    method: "POST",
                    body
                })
                .then(response => {
                    return response.json();
                })
                .then(async data => {
                    if(data.error) {
                        alert(data.error);
                    } else {
                        setUrls(await _urls.getData());

                        urlsUpdated();
                    }
                });

                break;

            case 'extension':
                body.append('name', extensionsState.name);
                body.append('url', extensionsState.url);
                extensionsState.images.forEach((image, index) => body.append('images_' + index, image));
                // body.append('images', fs.createReadStream(extensionsState.images), { filename: urlsState.images.name });

                // alert(JSON.stringify(extensionsState.images));
                fetch(dev ? 'http://localhost:8000/extensions' : 'https://feedsv2.7awi.com/casper/extensions', {
                    method: "POST",
                    body
                })
                .then(response => {
                    return response.json();
                })
                .then(async data => {
                    if(data.error) {
                        alert(data.error);
                    } else {
                        setExtensions(await _extensions.getData());

                        extensionsUpdated();
                    }
                });
                
                break;

            case 'tool':
                body.append('name', toolsState.name);
                body.append('url', toolsState.url);
                body.append('images', toolsState.images[0]);
                // body.append('images', fs.createReadStream(toolsState.images), { filename: urlsState.images.name });

                fetch(dev ? 'http://localhost:8000/tools' : 'https://feedsv2.7awi.com/casper/tools', {
                    method: "POST",
                    body
                })
                .then(response => {
                    return response.json();
                })
                .then(async data => {
                    if(data.error) {
                        alert(data.error);
                    } else {
                        setTools(await _tools.getData());

                        toolsUpdated();
                    }
                });

                break;
        
            default:
                break;
        }

        setState({ ...state, itemToRemove: -1 });
        setUrlsState({ ...urlsState, name: '', url: '', images: [] });
        setExtensionsState({ ...extensionsState, name: '', url: '', images: [] });
        setToolsState({ ...toolsState, name: '', url: '', images: [] });

        if(nameInput && nameInput.current) nameInput.current.value = '';
        if(urlInput && urlInput.current) urlInput.current.value = '';
        if(imagesInput && imagesInput.current) imagesInput.current.value = '';
    }

    const removeItemConfirmation = (index: number) => {
        handleShow();
        
        setState({ ...state, itemToRemove: index });
    }

    const removeItem = async () => {
        setState({ ...state, itemToRemove: -1})
        switch (state.type) {
            case 'url':
                await _urls.removeData(_urls.data ? _urls.data[state.itemToRemove].id : null);
                setUrls(await _urls.getData());

                setUrlsState({...urlsState, urls});
                urlsUpdated();
                handleClose();
                break;

            case 'extension':
                await _extensions.removeData(_extensions.data ? _extensions.data[state.itemToRemove].id : null);
                setExtensions(await _extensions.getData());

                setExtensionsState({...extensionsState, extensions});
                extensionsUpdated();
                handleClose();
                break;

            case 'tool':
                await _tools.removeData(_tools.data ? _tools.data[state.itemToRemove].id : null);
                setTools(await _tools.getData());

                setToolsState({...toolsState, tools});
                toolsUpdated();
                handleClose();
                break;
        
            default:
                break;
        }
    }

    const changeType = (type: string) => {
        setState({ type, itemToRemove: -1 });
        setUrlsState({ ...urlsState, name: '', url: '', images: [] });
        setExtensionsState({ ...extensionsState, name: '', url: '', images: [] });
        setToolsState({ ...toolsState, name: '', url: '', images: [] });

        nameInput.current.value = '';
        urlInput.current.value = '';
        imagesInput.current.value = '';
    }

    const updateName = (event: any) => {
        switch (state.type) {
            case 'url':
                setUrlsState({...urlsState, name: event.target.value})
                break;
            
            case 'extension':
                setExtensionsState({...extensionsState, name: event.target.value})
                break;

            case 'tool':
                setToolsState({...toolsState, name: event.target.value})
                break;
        
            default:
                break;
        }
    }

    const updateUrl = (event: any) => {
        switch (state.type) {
            case 'url':
                setUrlsState({...urlsState, url: event.target.value})
                break;
            
            case 'extension':
                setExtensionsState({...extensionsState, url: event.target.value})
                break;

            case 'tool':
                setToolsState({...toolsState, url: event.target.value})
                break;
        
            default:
                break;
        }
    }

    const updateImage = (event: any) => {
        switch (state.type) {
            case 'url':
                setUrlsState({...urlsState, images: [ ...urlsState.images, ...event.currentTarget.files ]})
                break;
            
            case 'extension':
                setExtensionsState({...extensionsState, images: [ ...extensionsState.images, ...event.currentTarget.files ]})
                break;

            case 'tool':
                setToolsState({...toolsState, images: [ ...toolsState.images, ...event.currentTarget.files ]})
                break;
        
            default:
                break;
        }
    }

    const getTypeState = () => {
        switch(state.type) {
            case 'url':
                return urlsState;
            case 'extension':
                return extensionsState;
            case 'tool':
                return toolsState;
        }
    }

    const getTypeStateSetter = () => {
        switch(state.type) {
            case 'url':
                return setUrlsState;
            case 'extension':
                return setExtensionsState;
            case 'tool':
                return setToolsState;
        }
    }

    const getTypeObjectState = () => {
        switch(state.type) {
            case 'url':
                return urls;
            case 'extension':
                return extensions;
            case 'tool':
                return tools;
        }
    }

    const changeUrlOrder = async (dir, index) => {

        console.log('urls', urls)
        if(index >= 0 && index <= urls.length) {
            if((index === extensions.length - 1 && dir === 'D') || (index === 0 && dir === 'U')) {
                return;
            }

            let __urls = urls;

            const urlToSwapIndex = dir === 'U' ? index - 1 : index + 1;

            const url = __urls[urlToSwapIndex];
            __urls[urlToSwapIndex] = __urls[index];
            __urls[index] = url;

            setUrls([...__urls]);

            await _urls.updateData(__urls[index].id, __urls[index].name, __urls[index].url, index);
            await _urls.updateData(__urls[urlToSwapIndex].id, __urls[urlToSwapIndex].name, __urls[urlToSwapIndex].url, urlToSwapIndex);

            urlsUpdated();
        }
    };

    const changeExtensionOrder = async (dir, index) => {
        if(index >= 0 && index <= extensions.length) {
            if((index === extensions.length - 1 && dir === 'D') || (index === 0 && dir === 'U')) {
                return;
            }

            let __extensions = extensions;

            const extensionToSwapIndex = dir === 'U' ? index - 1 : index + 1;

            const extension = __extensions[extensionToSwapIndex];
            __extensions[extensionToSwapIndex] = __extensions[index];
            __extensions[index] = extension;

            setExtensions([...__extensions]);

            await _extensions.updateData(__extensions[index].id, __extensions[index].name, __extensions[index].url, index);
            await _extensions.updateData(__extensions[extensionToSwapIndex].id, __extensions[extensionToSwapIndex].name, __extensions[extensionToSwapIndex].url, extensionToSwapIndex);

            extensionsUpdated();
        }
    };

    const changeToolOrder = async (dir, index) => {
        if(index >= 0 && index <= tools.length) {
            if((index === extensions.length - 1 && dir === 'D') || (index === 0 && dir === 'U')) {
                return;
            }

            let __tools = tools;

            const toolToSwapIndex = dir === 'U' ? index - 1 : index + 1;

            const tool = __tools[toolToSwapIndex];
            __tools[toolToSwapIndex] = __tools[index];
            __tools[index] = tool;

            setTools([...__tools]);

            await _tools.updateData(__tools[index].id, __tools[index].name, __tools[index].url, index);
            await _tools.updateData(__tools[toolToSwapIndex].id, __tools[toolToSwapIndex].name, __tools[toolToSwapIndex].url, toolToSwapIndex);

            toolsUpdated();
        }
    };

    const maxChar = 35;

    return (
        <div id='admin-area-super-container' style={{ display: show ? 'block' : 'none' }}>
            <div id='admin-area-container'>
                <div id='admin-area-close-button-container'>
                    <button onClick={() => toggleAdminArea()}>x</button>
                </div>

                <div id="admin-tabs-container">
                    <Button className="admin-tab" onClick={() => changeType('url')} variant={state.type === 'url' ? 'primary' : 'outline-primary'}>
                        URLs
                    </Button>

                    <Button className="admin-tab" onClick={() => changeType('extension')} variant={state.type === 'extension' ? 'primary' : 'outline-primary'}>
                        Extensions
                    </Button>

                    <Button className="admin-tab" onClick={() => changeType('tool')} variant={state.type === 'tool' ? 'primary' : 'outline-primary'}>
                        Tools
                    </Button>
                </div>

                <Container className='admin-section' id="admin-list-area">
                    <Form className='admin-section-form'>
                        {
                        state.type === 'url' ? 
                        urls.map((url, index) => {
                            return <Row key={index} className={`admin-section-row ${index % 2 === 0 ? 'even-row' : 'odd-row'}`}>
                                <Col className='admin-section-col' sm={1}>
                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button up' onClick={() => changeUrlOrder('U', index)}>
                                            <AiFillCaretUp className='icon' />
                                        </div>
                                    </div>

                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button down' onClick={() => changeUrlOrder('D', index)}>
                                            <AiFillCaretDown className='icon' />
                                        </div>
                                    </div>
                                </Col>

                                <Col className='admin-section-col' sm={1}>
                                    <Image size="sm" src={url.images[0]} height={20} />
                                </Col>

                                <Col className='admin-section-col' sm={4}>
                                    <Form.Control size="sm" value={url.name} disabled />
                                </Col>
                                <Col className='admin-section-col' sm={4}>
                                    <a href={url.url} target='_blank'>
                                        {url.url.length > maxChar ? url.url.slice(0, maxChar) + '...' : url.url}
                                    </a>
                                </Col>

                                <Col className='admin-section-col' sm={2}>
                                    <a className='btn btn-sm btn-info admin-section-mod-button' target='_blank' href={`https://feedsv2.7awi.com/socialeye/cms/admin/content-manager/collectionType/api::casper-url.casper-url/${url.id}`}>Edit</a>
                                </Col>
                            </Row>
                        })
                        :
                        state.type === 'extension' ? 
                        extensions.filter((extension, index) => extension.id !== 'all').map((url, index) => {
                            return <Row key={index} className={`admin-section-row ${index % 2 === 0 ? 'even-row' : 'odd-row'}`}>
                                <Col className='admin-section-col' sm={1}>
                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button up' onClick={() => changeExtensionOrder('U', index)}>
                                            <AiFillCaretUp className='icon' />
                                        </div>
                                    </div>

                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button down' onClick={() => changeExtensionOrder('D', index)}>
                                            <AiFillCaretDown className='icon' />
                                        </div>
                                    </div>
                                </Col>

                                <Col className='admin-section-col' sm={1}>
                                    <Image size="sm" src={url.images[0]} height={20} />
                                </Col>

                                <Col className='admin-section-col' sm={4}>
                                    <Form.Control size="sm" value={url.name} disabled />
                                </Col>
                                <Col className='admin-section-col' sm={4}>
                                    <a href={url.url} target='_blank'>
                                        {url.url.length > maxChar ? url.url.slice(0, maxChar) + '...' : url.url}
                                    </a>
                                </Col>

                                <Col className='admin-section-col' sm={2}>
                                    <a className='btn btn-sm btn-info admin-section-mod-button' target='_blank' href={`https://feedsv2.7awi.com/socialeye/cms/admin/content-manager/collectionType/api::casper-url.casper-url/${url.id}`}>Edit</a>
                                </Col>
                            </Row>
                        })
                        :
                        tools.map((url, index) => {
                            return <Row key={index} className={`admin-section-row ${index % 2 === 0 ? 'even-row' : 'odd-row'}`}>
                                <Col className='admin-section-col' sm={1}>
                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button up' onClick={() => changeToolOrder('U', index)}>
                                            <AiFillCaretUp className='icon' />
                                        </div>
                                    </div>

                                    <div className='admin-section-order-button-container'>
                                        <div className='admin-section-order-button down' onClick={() => changeToolOrder('D', index)}>
                                            <AiFillCaretDown className='icon' />
                                        </div>
                                    </div>
                                </Col>

                                <Col className='admin-section-col' sm={1}>
                                    <Image size="sm" src={url.images[0]} height={20} />
                                </Col>

                                <Col className='admin-section-col' sm={4}>
                                    <Form.Control size="sm" value={url.name} disabled />
                                </Col>
                                <Col className='admin-section-col' sm={4}>
                                    <a href={url.url} target='_blank'>
                                        {url.url.length > maxChar ? url.url.slice(0, maxChar) + '...' : url.url}
                                    </a>
                                </Col>

                                <Col className='admin-section-col' sm={2}>
                                    <a className='btn btn-sm btn-info admin-section-mod-button' target='_blank' href={`https://feedsv2.7awi.com/socialeye/cms/admin/content-manager/collectionType/api::casper-url.casper-url/${url.id}`}>Edit</a>
                                </Col>
                            </Row>
                        })}
                        {/* <hr />
                        <Row className={`admin-section-row ${index % 2 === 0 ? 'even-row' : 'odd-row'}`}>
                            <Col className='admin-section-col'>
                                    
                            </Col>
                            <Col className='admin-section-col'>
                                <Form.Control placeholder="Name (eg: Arabs Turbo)" ref={nameInput} onChange={(event) => updateName(event)} />
                            </Col>
                            <Col className='admin-section-col'>
                                <Form.Control placeholder="URL (eg: www.arabsturbo.com)" ref={urlInput} onChange={(event) => updateUrl(event)} />
                            </Col>
                            <Col className='admin-section-col'>
                                <Form.Control id="admin-section-add-images-input" ref={imagesInput} name="images[]" accept="image/*" style={{ display: 'none' }} type="file" multiple onChange={(event) => updateImage(event)} />
                                <Button id="admin-section-add-images-button" onClick={() => imagesInput.current.click()}>Add Images</Button>
                            </Col>

                            <Col className='admin-section-col'>
                                <Button className='admin-section-mod-button' variant="success" onClick={() => addItem()}>Add</Button>
                            </Col>
                        </Row> */}
                    </Form>
                        <div className='admin-section-images-list'>
                            {getTypeState().images.map((image, index) => {
                                return (
                                    <div className='admin-section-image-container'>
                                        <img className='admin-section-image' src={ URL.createObjectURL(image) } />
                                        <div className='admin-section-image-name'>
                                            {image.name.split(/(\\|\/)/g).pop()}
                                        </div>
                                        <div className='admin-section-image-remove-button' onClick={() => {
                                            const images = getTypeState().images;
                                            images.splice(index, 1);
                                            getTypeStateSetter()({...getTypeState(), images})
                                        }}>
                                            <div>
                                            x
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                </Container>
            </div>

            {/* <Modal
                show={showConfirmation}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Confirm</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {`Are you sure you wish to remove ${state.itemToRemove > -1 && getTypeObjectState()[state.itemToRemove].name}?`}
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        No
                    </Button>
                    <Button variant="success" onClick={() => removeItem()}>Yes</Button>
                </Modal.Footer>
            </Modal> */}
        </div>
    )
}

export default Admin;