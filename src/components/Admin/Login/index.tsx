import React, { useState } from "react";
import { Button, Container, Col, Form, Row, Modal } from "react-bootstrap";

import './login.css';

const Login = ({ show, login, closeLoginArea }: any) => {
    const [state, setState] = useState({ username: '', password: '' });

    const doLogin = () => {
        setState({ ...state, username: '', password: ''})
        login(state.username, state.password);
    }

    return (
        <div id='login-area-super-container' style={{ display: show ? 'block' : 'none' }}>
            <div id="login-area-container">
                <h3>Please login to access the admin area</h3>
                <Form id='login-area-form'>
                    <Form.Control className='login-field' placeholder="Username" value={state.username} onChange={ (event) => setState({ ...state, username: event.target.value })} />
                    <Form.Control className='login-field' placeholder="Password" type="password" value={state.password} onChange={ (event) => setState({ ...state, password: event.target.value })} />

                    <div id="login-button-container">
                        <Button className="login-tab" variant='danger' onClick={closeLoginArea}>
                            Cancel
                        </Button>
            
                        <Button className="login-tab" variant='success' onClick={doLogin}>
                            Login
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    )
}

export default Login;