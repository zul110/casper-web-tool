import React from "react";
import { Container, Row, Col } from 'react-bootstrap';
// import tools from './tools';
import './tools.css';

// const exec = window.require("child_process").exec;

const OnlineTools = ({ tools }: any) => {
    const goToUrl = (url: string) => {
        const win = window.open(url, '_blank');
        win!.focus();
        // const command = `start chrome ${url}`;
        // exec(command);
    }

    return (
        <Container id='tools-list-container' fluid>
            <Row id='tools-list-row'>
                {tools.map((tool: any, index: number) =>
                    <Col key={index} xs={6} md={2} className='tool-item-container' onClick={() => goToUrl(tool.url)}>
                        <div className='tool-item'>
                            <div className="tool-logo-container">
                                <img src={tool.images[0]} className='tool-logo' />
                            </div>

                            <div className="tool-title-container">
                                {tool.name}
                                {tool.name.length < 30 ? <div>&nbsp;</div> : ''}
                            </div>
                        </div>
                    </Col>
                    )
                }
            </Row>
        </Container>
    )
}

export default OnlineTools;