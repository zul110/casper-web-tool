import React, { useState, useEffect } from "react";
import { Container, Row, Col, Card } from 'react-bootstrap';
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';
import './tabs.css';

import _7awi from '../../../static/images/logos/7awi.png';
import { LeftArrow, RightArrow } from '../../Arrows';

// const exec = window.require("child_process").exec;

const _7awiTabs = ({ urls }: any) => {
    const openChrome = (url: string) => {
        if(url && url.trim() !== '') {
            const win = window.open(!url.startsWith('https://') && !url.startsWith('http://') ? `https://${url}` : url, '_blank');
            win!.focus();
        } else {
            urls.forEach((url: any) => {
                const win = window.open(!url.url.startsWith('https://') && !url.url.startsWith('http://') ? `https://${url.url}` : url.url, '_blank');
                win!.focus();
            });
        }
    };

    return (
        <div id='tabs-container'>
            <div className='urls-grid'>
                {[...urls, { url: '', images: [], name: 'LAUNCH ALL' }].map((url) => (
                    <Card
                        className='tab-item-container'
                        key={url.url}
                        title={url.url}
                        onClick={() => openChrome(url.url)}
                    >
                        <div className="tab-item-logo">
                            <img src={url.images[0]} />
                        </div>

                        {
                            url.name === 'LAUNCH ALL'
                            &&
                            <div className='tab-item-label'>
                                {url.name}
                            </div>
                        }
                    </Card>
                ))}
            </div>

            {/* <ScrollMenu scrollContainerClassName='urls-scroll-menu' LeftArrow={LeftArrow} RightArrow={RightArrow}>
                {urls.map((url) => (
                    <Card
                        className='tab-item-container'
                        itemId={url.url}
                        key={url.url}
                        title={url.url}
                        onClick={() => openChrome(url.url)}
                        // onClick={handleClick(id)}
                        // selected={isItemSelected(id)}
                    >
                        <div className="tab-item-logo">
                            <img src={url.images[0]} />
                        </div>
                    </Card>
                ))}
            </ScrollMenu> */}


            {/* <Container id='tabs-list-container'>
                <Row id='tabs-list-row'>
                    {
                        urls.map((url: any) => <Col className='tab-item-container' sm={2} md={1} onClick={() => openChrome(url.url)}>
                                <div className="tab-item-logo">
                                    <img src={url.images[0]} />
                                </div>
                        </Col>)
                    }

                    <Col className='tab-item-container tab-item-launch-all-container' sm={2} md={1} onClick={() => openChrome('')}>
                        <h6>Launch all</h6>
                    </Col>
                </Row>
            </Container> */}
        </div>
    )
}

export default _7awiTabs;