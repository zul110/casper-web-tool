const dev = true;
const devBase = 'http://localhost:8000';
const prodBase = 'https://feedsv2.7awi.com/casper';

const base = dev ? devBase : prodBase;

const url = 'https://feedsv2.7awi.com/socialeye/cms/api/casper-urls?populate=images&filters[type][$eq]=tab';

const urls = {
    data: [],
    getData: async () => {
        try {
            const response = await fetch(url);
            const data = (await response.json()).data.map((d: any) => {
                return {
                    id: d.id,
                    ...d.attributes,
                    url: !d.attributes.url.startsWith('https://') ? `https://${d.attributes.url}` : d.attributes.url,
                    images: d.attributes.images.data.map((_d: any) => {
                        return `https://feedsv2.7awi.com/socialeye/cms${_d.attributes.url}`
                    })
                };
            });

            console.log(data)

            urls.data = data
            .sort((a: any, b: any) => (a.order || 0) - (b.order || 0));

            return data;
        } catch(err) {
            alert(err)
            throw err;
        }
    },
    updateData: async (id: number, name: string, _url: string, order: number) => {
        try {
            const url = 'https://feedsv2.7awi.com/socialeye/cms/api/casper-urls';
            const response = await fetch(`${url}/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    data: {
                        name,
                        url: _url,
                        order
                    }
                })
            });

            const data = await response.json();

            return data;
        } catch(err) {
            alert(err);
            throw err;
        }
    },
    removeData: async (id: string) => {
        // const url = `http://20.71.169.36:8000/urls/${id}`;
        const url = `${base}/urls/${id}`;

        try {
            const response = await fetch(url, {
                method: 'DELETE'
            });
            const data = await response.json();
        } catch(err) {
            alert(err)
            throw err;
        }
    }
}

export default urls;