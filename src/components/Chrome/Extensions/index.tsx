import React, { useState, useEffect, useRef } from "react";
// import _extensions from './extensions';
import { Container, Row, Col, Carousel, Card } from 'react-bootstrap';
import './extensions.css';
import casperImage from '../../../static/images/extensions/casper/1.jpg';

// const exec = window.require("child_process").exec;

const Extensions = ({ extensions }: any) => {
    const carousel = useRef(null);
    const extensionSelected = (extension: any) => {
        setState({ extension });
    }

    const openChrome = (extension: any) => {
        // const { extension: id } = state;
        // setState({ extension: id });
        // const extension = (extensions.find((extension: any) => extension.id === id))?.url;
        const win = window.open(extension.url, '_blank');
        win!.focus();
        // const command = extension && extension !== '' ? `start chrome ${extension}` : `start chrome ${extensions.map((extension: any) => extension.url).join(' ')}`;
        // exec(command);
    }

    const extensionHovered = (extension: string) => {
        setState({ extension });
    }

    const [state, setState] = useState({ extension: '' });

    useEffect(() => {
        if(extensions && extensions[0]) extensionSelected(extensions[0].id);
    }, [extensions]);

    const images = extensions.map((extension: any) => extension.images[0]);

    return (
        <div id='extensions-container'>
            <Carousel id="extension-items-description-carousel" ref={carousel}>
                {
                extensions.map((extension: any, index: number) => {
                    return (
                    <Carousel.Item className={'extension-carousel-item'}>
                        <img
                        className={state.extension === 'casper' || state.extension === 'all' ? 'extension-image extension-image-contain' : 'extension-image'}
                        src={extension.images[0]}
                        />
                        <div className='extensions-carousel-caption-container'>
                            <span className='extensions-carousel-caption'>
                                {extension.name}
                            </span>
                            <button className="get-extension-button" onClick={() => openChrome(extension)}>Get</button>
                        </div>
                    </Carousel.Item>
                    )
                })}
            </Carousel>
            
            {/* <Row>
                <Col xs={12} md={3} id="extension-items-list-container">
                    <ul id="extension-items-list">
                        {extensions.filter((extension: any) => extension.id !== 'all').map((extension: any) => <li className="extension-item" onClick={() => extensionSelected(extension.id)}>
                                <div className="extension-item-label">
                                    {extension.name}
                                </div>
                            </li>
                        )}

                        <li className="extension-item" onClick={() => extensionSelected('all')}>
                            <div className="extension-item-label">
                                All
                            </div>
                        </li>
                    </ul>
                </Col>
                <Col xs={12} md={9}  id="extension-items-description-container">
                    <Carousel id="extension-items-description-carousel" ref={carousel}>
                        {
                        extensions.find((extension: any) => state.extension === extension.id)
                        ?.images?.map((image: string, index: number) => {
                            return <Carousel.Item className={index === 0 ? 'extension-carousel-item active' : 'extension-carousel-item'}>
                            <img
                            className={state.extension === 'casper' || state.extension === 'all' ? 'extension-image extension-image-contain' : 'extension-image'}
                            src={image}
                            />
                            <Carousel.Caption>
                                <button className="get-extension-button" onClick={openChrome}>Get</button>
                            </Carousel.Caption>
                        </Carousel.Item>
                        })}
                    </Carousel>
                </Col>
            </Row> */}
        </div>
    )
}

export default Extensions;