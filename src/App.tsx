import React, { useState, useEffect } from "react";
import localforage from 'localforage';

import logo from './logo.svg';
import './App.css';
import OnlineTools from './components/OnlineTools';
import _7awiTabs from './components/Chrome/7awiTabs';
import Extensions from './components/Chrome/Extensions';

import Admin from './components/Admin';
import Login from './components/Admin/Login';

import urls from './components/Chrome/7awiTabs/urls';
import extensions from './components/Chrome/Extensions/extensions';
import tools from './components/OnlineTools/tools';

import adminicon from './static/images/icons/adminicon-256.png';

// const fs = require("fs");
// const exec = window.require("child_process").exec;
// const path = require("path");

const App = () => {
  // const installFonts = () => {
  //   const folder = 'src/static/fonts';
  //   const fonts = fs.readdirSync(folder);

  //   fonts.forEach((fontName: string) => {
  //     let dir = path.resolve('.');
  //     const font = `${dir}\\${folder.replace(/\//g, '\\')}\\${fontName}`;
  //     dir = '%WINDIR%';
  //     const install = `reg add "HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts" /v "${font} (TrueType)" /t REG_SZ /d ${font} /f`;
  //   });
  // }

  // installFonts();

  const [state, setState] = useState({ urls: [], extensions: [], tools: [], showAdmin: false, showLogin: false, isLoggedIn: false });

  useEffect(() => {
      (async () => {
        let isLoggedIn = 'false';
        try {
            isLoggedIn = await localforage.getItem('isLoggedIn') || 'false';
        } catch (err) {
            console.log(err);
        }
        
        setState({ ...state, isLoggedIn: isLoggedIn == 'true', urls: await urls.getData(), extensions: await extensions.getData(), tools: await tools.getData() });
      })();
  }, []);

  const urlsUpdated = async () => {
    setState({ ...state, urls: await urls.getData() });
  }

  const extensionsUpdated = async () => {
    setState({ ...state, extensions: await extensions.getData() });
  }

  const toolsUpdated = async () => {
    setState({ ...state, tools: await tools.getData() });
  }

  const toggleAdminArea = () => {
    if(!state.isLoggedIn) {
      setState({ ...state, showLogin: !state.showLogin });
    } else {
      setState({ ...state, showAdmin: !state.showAdmin });
    }
  }

  const closeLoginArea = () => {
    setState({ ...state, showLogin: false });
  }

  const login = async (username: string, password: string) => {
    if(username === 'admin' && password === 'admin') {
      await localforage.setItem('isLoggedIn', 'true');
      setState({ ...state, isLoggedIn: true, showAdmin: true, showLogin: false });
    } else {
      alert('Invalid username and/or password. Please try again.')
    }
  }
  
  return (
    <div style={{ height: '100vh', margin: '-12px', paddingBottom: '20px', overflowY: state.showAdmin ? 'hidden' : 'auto'}}>

      <div className='section-super-container' id="extensions-super-container">
        {/* <h2 className='section-label' id="extensions-label">
          Helpful Extensions
        </h2> */}

      <div className='section-super-container' id="tabs-super-container">
        <h2 className='section-label' id="tabs-label">
          Casper Studio Tools
        </h2>
        <div id="tabs-container">
          <_7awiTabs urls={state.urls} />
        </div>
      </div>

        <div id="extensions-container">
          <h2 className='section-label' id="tools-label">
            Helpful Extensions
          </h2>
          <OnlineTools tools={state.extensions} />
        </div>
      </div>

      <div className='section-super-container' id="tools-super-container">
        <h2 className='section-label' id="tools-label">
          Useful Tools
        </h2>
        <div id="tools-container">
          <OnlineTools tools={state.tools} />
        </div>
      </div>

      <div id="footer">
        Copyright &copy; 7awi.com
      </div>

      <Admin show={state.showAdmin} toggleAdminArea={toggleAdminArea} urlsUpdated={urlsUpdated} extensionsUpdated={extensionsUpdated} toolsUpdated={toolsUpdated} />
      <Login show={state.showLogin} login={login} closeLoginArea={closeLoginArea} />

      <div id="admin-button-container">
        <button id="admin-button" onClick={() => toggleAdminArea()}>
          <img id="admin-button-image" src={adminicon} />
        </button>
      </div>
    </div>
  );
}

export default App;
